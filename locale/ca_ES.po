# 
msgid ""
msgstr "Content-Type: text/plain; charset=utf-8\n"

msgctxt "error:galatea.blog.post:"
msgid "\"%(file_name)s\" file mime is not an image (jpg, png or gif)"
msgstr "El fitxer \"%(file_name)s\" no es una imatge (jpg, png or gif)"

msgctxt "error:galatea.blog.post:"
msgid "Not know file mime \"%(file_name)s\""
msgstr "No es coneix el tipus del fitxer \"%(file_name)s\""

msgctxt "error:galatea.blog.post:"
msgid "Thumb \"%(file_name)s\" size is larger than \"%(size)s\"Kb"
msgstr "El tamany de la miniatura \"%(file_name)s\" es mes gran que \"%(size)s\"Kb"

msgctxt "error:galatea.blog.post:"
msgid ""
"You can not delete posts because you will get error 404 NOT Found. Dissable "
"active field."
msgstr ""
"No pot eliminar posts perque obtindria una error 404 NOT Found. Desactiva'l."

msgctxt "field:galatea.blog.comment,active:"
msgid "Active"
msgstr "Actiu"

msgctxt "field:galatea.blog.comment,comment_create_date:"
msgid "Create Date"
msgstr "Data creació"

msgctxt "field:galatea.blog.comment,create_date:"
msgid "Create Date"
msgstr "Data creació"

msgctxt "field:galatea.blog.comment,create_uid:"
msgid "Create User"
msgstr "Usuari creació"

msgctxt "field:galatea.blog.comment,description:"
msgid "Description"
msgstr "Descripció"

msgctxt "field:galatea.blog.comment,id:"
msgid "ID"
msgstr "ID"

msgctxt "field:galatea.blog.comment,post:"
msgid "Post"
msgstr "Post"

msgctxt "field:galatea.blog.comment,rec_name:"
msgid "Name"
msgstr "Nom"

msgctxt "field:galatea.blog.comment,user:"
msgid "User"
msgstr "Usuari"

msgctxt "field:galatea.blog.comment,write_date:"
msgid "Write Date"
msgstr "Data modificació"

msgctxt "field:galatea.blog.comment,write_uid:"
msgid "Write User"
msgstr "Usuari modificació"

msgctxt "field:galatea.blog.post,active:"
msgid "Active"
msgstr "Actiu"

msgctxt "field:galatea.blog.post,attachments:"
msgid "Attachments"
msgstr "Adjunts"

msgctxt "field:galatea.blog.post,comment:"
msgid "Comment"
msgstr "Comentari"

msgctxt "field:galatea.blog.post,comments:"
msgid "Comments"
msgstr "Comentaris"

msgctxt "field:galatea.blog.post,create_date:"
msgid "Create Date"
msgstr "Data creació"

msgctxt "field:galatea.blog.post,create_uid:"
msgid "Create User"
msgstr "Usuari creació"

msgctxt "field:galatea.blog.post,description:"
msgid "Description"
msgstr "Descripció"

msgctxt "field:galatea.blog.post,galatea_website:"
msgid "Website"
msgstr "Lloc web"

msgctxt "field:galatea.blog.post,gallery:"
msgid "Gallery"
msgstr "Galeria"

msgctxt "field:galatea.blog.post,id:"
msgid "ID"
msgstr "ID"

msgctxt "field:galatea.blog.post,long_description:"
msgid "Long Description"
msgstr "Descripció llarga"

msgctxt "field:galatea.blog.post,metadescription:"
msgid "Meta Description"
msgstr "Meta Description"

msgctxt "field:galatea.blog.post,metakeywords:"
msgid "Meta Keywords"
msgstr "Meta Keywords"

msgctxt "field:galatea.blog.post,metatitle:"
msgid "Meta Title"
msgstr "Meta Title"

msgctxt "field:galatea.blog.post,name:"
msgid "Title"
msgstr "Títol"

msgctxt "field:galatea.blog.post,post_create_date:"
msgid "Create Date"
msgstr "Data creació"

msgctxt "field:galatea.blog.post,post_write_date:"
msgid "Write Date"
msgstr "Data modificació"

msgctxt "field:galatea.blog.post,rec_name:"
msgid "Name"
msgstr "Nom"

msgctxt "field:galatea.blog.post,slug:"
msgid "slug"
msgstr "slug"

msgctxt "field:galatea.blog.post,slug_langs:"
msgid "Slug Langs"
msgstr "Slug idiomes"

msgctxt "field:galatea.blog.post,template:"
msgid "Template"
msgstr "Plantilla"

msgctxt "field:galatea.blog.post,thumb:"
msgid "Thumb"
msgstr "Miniatura"

msgctxt "field:galatea.blog.post,thumb_filename:"
msgid "File Name"
msgstr "Nom fitxer"

msgctxt "field:galatea.blog.post,thumb_path:"
msgid "Thumb Path"
msgstr "Ruta miniatura"

msgctxt "field:galatea.blog.post,total_comments:"
msgid "Total Comments"
msgstr "Total comentaris"

msgctxt "field:galatea.blog.post,uri:"
msgid "Uri"
msgstr "Uri"

msgctxt "field:galatea.blog.post,user:"
msgid "User"
msgstr "Usuari"

msgctxt "field:galatea.blog.post,visibility:"
msgid "Visibility"
msgstr "Visibilidad"

msgctxt "field:galatea.blog.post,write_date:"
msgid "Write Date"
msgstr "Data modificació"

msgctxt "field:galatea.blog.post,write_uid:"
msgid "Write User"
msgstr "Usuari modificació"

msgctxt "field:galatea.configuration,blog_thumb_crop:"
msgid "Blog Thumb Crop"
msgstr "Talla miniatura blog"

msgctxt "field:galatea.configuration,blog_thumb_size:"
msgid "Blog Thumb Size"
msgstr "Tamany miniatura blog"

msgctxt "field:galatea.website,blog_anonymous:"
msgid "Blog Anonymous"
msgstr "Blog anònim"

msgctxt "field:galatea.website,blog_anonymous_user:"
msgid "Blog Anonymous User"
msgstr "Blog usuari anònim"

msgctxt "field:galatea.website,blog_comment:"
msgid "Blog comments"
msgstr "Comentaris blog"

msgctxt "help:galatea.blog.comment,active:"
msgid "Dissable to not show content post."
msgstr "Desactivar per no mostrar contingut post."

msgctxt "help:galatea.blog.comment,description:"
msgid ""
"You could write wiki markup to create html content. Formats text following "
"the MediaWiki (http://meta.wikimedia.org/wiki/Help:Editing) syntax."
msgstr ""
"Pot escriure amb els tags de wiki per formatejar contingut HTML. Pot "
"consultar la xuleta a MediaWiki "
"(http://meta.wikimedia.org/wiki/Help:Editing)."

msgctxt "help:galatea.blog.post,active:"
msgid "Dissable to not show content post."
msgstr "Desactivar per no mostrar contingut post."

msgctxt "help:galatea.blog.post,comment:"
msgid "Active comments."
msgstr "Activar comentaris."

msgctxt "help:galatea.blog.post,description:"
msgid ""
"You could write wiki markup to create html content. Formats text following "
"the MediaWiki (http://meta.wikimedia.org/wiki/Help:Editing) syntax."
msgstr ""
"Pot escriure amb els tags de wiki per formatejar contingut HTML. Pot "
"consultar la xuleta a MediaWiki "
"(http://meta.wikimedia.org/wiki/Help:Editing)."

msgctxt "help:galatea.blog.post,gallery:"
msgid "Active gallery attachments."
msgstr "Activar galeria."

msgctxt "help:galatea.blog.post,long_description:"
msgid ""
"You could write wiki markup to create html content. Formats text following "
"the MediaWiki (http://meta.wikimedia.org/wiki/Help:Editing) syntax."
msgstr ""
"Pot escriure amb els tags de wiki per formatejar contingut HTML. Pot "
"consultar la xuleta a MediaWiki "
"(http://meta.wikimedia.org/wiki/Help:Editing)."

msgctxt "help:galatea.blog.post,metadescription:"
msgid ""
"Almost all search engines recommend it to be shorter than 155 characters of "
"plain text"
msgstr ""
"La majoria dels cercadors recomanen un text de 155 caràcters de text pla."

msgctxt "help:galatea.blog.post,metakeywords:"
msgid "Separated by comma"
msgstr "Separat amb coma"

msgctxt "help:galatea.blog.post,slug:"
msgid "Cannonical uri."
msgstr "Cannonical uri."

msgctxt "help:galatea.blog.post,thumb_filename:"
msgid "Thumbnail File Name"
msgstr "Nom miniatura"

msgctxt "help:galatea.configuration,blog_thumb_crop:"
msgid "Crop Thumb Blog Image"
msgstr "Talla la imatge miniatura blog"

msgctxt "help:galatea.configuration,blog_thumb_size:"
msgid "Thumbnail Blog Image Size (width x height)"
msgstr "Tamany imatge miniatura bog (ample x alt)"

msgctxt "help:galatea.website,blog_anonymous:"
msgid "Active user anonymous to publish comments."
msgstr "Activar usuaris anònims per publicar comentaris."

msgctxt "help:galatea.website,blog_comment:"
msgid "Active blog comments."
msgstr "Activar comentaris al blog"

msgctxt "model:galatea.blog.comment,name:"
msgid "Blog Comment Post"
msgstr "Blog comentari post"

msgctxt "model:galatea.blog.post,name:"
msgid "Blog Post"
msgstr "Post blog"

msgctxt "model:ir.action,name:act_blog_comment_form"
msgid "Comments"
msgstr "Comentaris"

msgctxt "model:ir.action,name:act_blog_post_comment_form2"
msgid "Comments"
msgstr "Comentaris"

msgctxt "model:ir.action,name:act_blog_post_form"
msgid "Posts"
msgstr "Posts"

msgctxt "model:ir.ui.menu,name:menu_blog_comment_form"
msgid "Comments"
msgstr "Comentaris"

msgctxt "model:ir.ui.menu,name:menu_blog_post_form"
msgid "Posts"
msgstr "Posts"

msgctxt "model:ir.ui.menu,name:menu_galatea_blog"
msgid "Blog"
msgstr "Blog"

msgctxt "model:res.group,name:group_galatea_blog"
msgid "Galatea Blog"
msgstr "Galatea Blog"

msgctxt "selection:galatea.blog.post,visibility:"
msgid "Manager"
msgstr "Gestor"

msgctxt "selection:galatea.blog.post,visibility:"
msgid "Public"
msgstr "Públic"

msgctxt "selection:galatea.blog.post,visibility:"
msgid "Register"
msgstr "Registrat"

msgctxt "view:galatea.blog.comment:"
msgid "Comment"
msgstr "Comentari"

msgctxt "view:galatea.blog.comment:"
msgid "Comments"
msgstr "Comentaris"

msgctxt "view:galatea.blog.comment:"
msgid "Description"
msgstr "Descripció"

msgctxt "view:galatea.blog.comment:"
msgid "Time"
msgstr "Hora"

msgctxt "view:galatea.blog.post:"
msgid "Attachments"
msgstr "Adjunts"

msgctxt "view:galatea.blog.post:"
msgid "Descriptions"
msgstr "Descripcions"

msgctxt "view:galatea.blog.post:"
msgid "Extra"
msgstr "Extra"

msgctxt "view:galatea.blog.post:"
msgid "Info"
msgstr "Info"

msgctxt "view:galatea.blog.post:"
msgid "Post"
msgstr "Post"

msgctxt "view:galatea.blog.post:"
msgid "Posts"
msgstr "Posts"

msgctxt "view:galatea.blog.post:"
msgid "SEO"
msgstr "SEO"

msgctxt "view:galatea.website:"
msgid "Blog"
msgstr "Blog"
